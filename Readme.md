# Wubix Package Registry

A registry of programs for Wubix, used by [Wubix Package Manager][wpm].

[wpm]: https://gitlab.com/wubix-crates/wubix-package-manager

## Managing packages in the registry

Each package in the registry has its own file in [`packages/`](./packages])
in TOML format. Its structure is like this:

```toml
name = "package-name"
description = "The package's description"
version = "1.2.3"
repository = "https://gitlab.com/wubix-crates/package's-repo.git"
commit = "27b7a36b8608d8d07ad121fec4cd4509fa7bd298"

[executables.cat]
copy_from = "cat/cat.wef"
copy_to = "/bin"

[executables.ping]
copy_from = "ping/ping.wef"
copy_to = "/usr/bin"
set_uid = true
```

`name`, `description` and `version` are self-explanatory. `repository` is a URL
to a Git repository which can be used for cloning. When it's cloned, one must
be able to run `npm run build` in the repository's root and expect every
executable to be built. To ensure that the package in the registry will stay
the same until `version` is changed, you need to pin-point the commit at which
the package is built. (By the way, `description` is an optional field, all
others are required though.)

`executables` is a map of executable names and metadata about the executable.
`copy_from` is the path of the executable in the repository after building,
`copy_to` is the directory where the executable is put inside Wubix.
`set_uid`, an optional field, defines whether the executable needs to have the
`setuid` permission bit set.

## Building the registry

Since there are no known successful attempts at running `rustc` inside Wubix,
packages need to be built on real machines. This repository contains a program
that builds every package and compiles the final registry. To build the
registry, just run `cargo run`. After that, the created `registry` directory
will be ready to be deployed somewhere and used by clients. (In the `builds`
directory, the builder clones and builds all the packages, and if not deleted,
the builder will reuse it and avoid recompiling everything from scratch).

In case of this repository, building is done on GitLab CI and the registry is
deployed via Gitlab Pages and available at <https://wubix-crates.gitlab.io/wubix-package-registry>.

## The structure of the built registry

In the root, there's a file named `registry.json` of the following schema
(in TypeScript-speak):

```typescript
interface Registry {
  packages: string[];
}
```

The `packages` field is just an array of the names of all packages. Then, there
is a directory named `packages` where every subdirectory is named after the
contained package.

Say, the package is named `coreutils`. There'll be a file named
`packages/coreutils/info.json` of the following schema:

```typescript
interface PackageInfo {
  description?: string;
  version?: string;
  executables: { [executable: string]: Executable };
}

interface Executable {
  copy_to: string;
  set_uid: boolean;
}
```

All these fields reflect the ones from the package's TOML file.

In `packages/coreutils`, there'll also be a subdirectory `executables` where
all the executables are collected. If `coreutils` provides an executable `echo`,
then a client will be able to download it from `packages/coreutils/executables/echo`.
