use git2::{ErrorCode, Oid, Repository};
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};
use std::{
    collections::HashMap,
    error::Error,
    fs::{copy, create_dir_all, read, read_dir, remove_dir_all, write},
    io::{stdout, Write},
    path::Path,
    process::Command,
};

fn default_permissions() -> u32 {
    0o644
}

#[derive(Debug, Deserialize)]
struct File {
    copy_from: String,
    #[serde(default)]
    uid: u32,
    #[serde(default)]
    gid: u32,
    #[serde(default = "default_permissions")]
    permissions: u32,
}

#[derive(Debug, Deserialize)]
struct Package {
    name: String,
    description: Option<String>,
    version: String,
    repository: String,
    commit: String,
    files: HashMap<String, File>,
}

#[derive(Debug, Serialize)]
struct BuiltFile {
    fingerprint: String,
    uid: u32,
    gid: u32,
    permissions: u32,
}

#[derive(Debug, Serialize)]
struct BuiltPackage {
    description: Option<String>,
    version: String,
    files: HashMap<String, BuiltFile>,
}

#[derive(Debug, Serialize)]
struct Registry {
    packages: Vec<String>,
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut registry = Registry {
        packages: Vec::new(),
    };

    let mut has_errored = false;
    for entry in read_dir("packages")? {
        let entry = entry?;
        let file = read(entry.path())?;
        let package: Package = toml::de::from_slice(&file)?;
        let name = package.name.clone();

        match build_package(package) {
            Ok(()) => registry.packages.push(name),
            Err(error) => {
                println!("Error building: {}", error);
                has_errored = true;
            }
        }
    }

    if has_errored {
        std::process::exit(1);
    }

    let registry_json = serde_json::to_vec(&registry)?;
    write("registry/registry.json", registry_json)?;

    Ok(())
}

fn build_package(package: Package) -> Result<(), Box<dyn Error>> {
    println!("Building {}:", package.name);

    print!("- Cloning {}...", package.repository);
    let _ = stdout().flush();

    let repository_path = Path::new("builds").join(&package.name);
    let mut needs_fetching = false;
    let repository = match Repository::clone(&package.repository, &repository_path) {
        Ok(repository) => {
            println!(" done");
            repository
        }
        Err(error) if error.code() == ErrorCode::Exists => {
            needs_fetching = true;
            println!(" already cloned");
            Repository::open(&repository_path)?
        }
        Err(error) => {
            println!(" error");
            return Err(error.into());
        }
    };

    if needs_fetching {
        fetch(&repository)?;
    }

    print!("- Checking out {}...", package.commit);
    let _ = stdout().flush();
    let oid = Oid::from_str(&package.commit)?;
    let commit = repository.find_object(oid, None)?;
    repository.checkout_tree(&commit, None)?;
    repository.set_head_detached(oid)?;
    println!(" done");

    println!("- Running `npm run build`...");
    let exit_status = Command::new("npm")
        .args(&["run", "build"])
        .current_dir(&repository_path)
        .spawn()?
        .wait()?;

    if !exit_status.success() {
        return Err(format!("`npm run build` exited with code {}", exit_status).into());
    }

    let package_directory = Path::new("registry/packages/").join(&package.name);
    let files_directory = package_directory.join("files");
    let _ = remove_dir_all(&package_directory);
    create_dir_all(&files_directory)?;

    let mut built_package = BuiltPackage {
        description: package.description.map(|x| x.trim().to_owned()),
        version: package.version,
        files: HashMap::new(),
    };

    for (file_path, file) in package.files {
        print!("- Collecting {}...", file_path);
        let _ = stdout().flush();

        let copy_from = repository_path.join(&file.copy_from);
        let fingerprint = format!("{:x}", Sha256::digest(&read(&copy_from)?));
        let path_in_registry = files_directory.join(&fingerprint);
        copy(copy_from, path_in_registry)?;

        built_package.files.insert(
            file_path,
            BuiltFile {
                fingerprint,
                uid: file.uid,
                gid: file.gid,
                permissions: file.permissions,
            },
        );

        println!(" done");
    }

    let built_package_json = serde_json::to_vec(&built_package)?;
    write(package_directory.join("info.json"), built_package_json)?;

    Ok(())
}

fn fetch(repository: &Repository) -> Result<(), Box<dyn Error>> {
    print!("- Fetching...");
    let _ = stdout().flush();

    repository
        .find_remote("origin")?
        .fetch::<&str>(&[], None, None)?;
    println!(" done");

    Ok(())
}
